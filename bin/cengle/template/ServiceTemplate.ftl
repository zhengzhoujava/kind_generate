package ${bussPackage}.${lowerName}.service#if($!entityPackage).${entityPackage}#end;

import java.util.List;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import ${bussPackage}.${lowerName}.domain.${classNameDO};

/**
 * ${codeName}业务处理接口<br/>
 *
 * @Date: ${dateTime}
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface ${className}Service {

    /**
     * 分页查询
     * @param pageQuery
     * @return
     */
	PageView<${classNameDO}> selectPageList(PageQuery pageQuery);

    /**
     * 保存数据
     * @param entity
     */
	int save(${classNameDO} entity);

    /**
     * 获取数据对象
     * @param id
     * @return
     */
    ${classNameDO} getById(Long id);

    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<${classNameDO}> queryList(${classNameDO} entity);
	
}
