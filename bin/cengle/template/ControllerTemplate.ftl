package ${web_package}.${lowerName};

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kind.common.client.response.JsonResponseResult;
import com.kind.common.contants.Constants;
import com.kind.common.dto.DataGridResult;
import com.kind.common.enums.ResponseState;
import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageView;
import com.kind.common.uitls.DateUtils;
import com.kind.common.uitls.NumberUtils;
import ${bussPackage}.${lowerName}.domain.${className}DO;
import ${bussPackage}.${lowerName}.service.${className}Service;
import ${bussPackage}.system.domain.FileDO;
import ${bussPackage}.system.domain.TableCustomTempletDO;
import ${bussPackage}.system.service.CoFileService;
import ${bussPackage}.system.service.TableCustomService;
import ${bussPackage}.system.service.TableCustomTempletService;
import ${web_package}.common.ExcelUtils;
import ${web_package}.common.controller.BaseController;

/**
 * 
 * Function:${codeName}管理控制器. <br/>
 * 
 * @date:2016年5月12日 上午11:18:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller
@RequestMapping("${lowerName}/${lowerName}")
public class ${className}Controller extends BaseController {
	
	@Autowired
	private ${className}Service ${lowerName}Service;
	
	@Autowired
	private CoFileService coFileService;
	
	@Autowired
	private TableCustomService tableCustomService;
	
	@Autowired
	private TableCustomTempletService tableCustomTempletService;

    /**列表页面*/
    private final String LIST_PAGE = "${lowerName}/${lowerName}_list";
    /**表单页面*/
    private final String FORM_PAGE = "${lowerName}/${lowerName}_form";

	/**
	 * 进入到默认列表页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String toListPage() {
		return LIST_PAGE;
	}

	/**
	 * 获取分页查询列表数据.
	 */
	@RequestMapping(value = "selectPageList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridResult selectPageList(${className}DO query, HttpServletRequest request) {
//		String sort = request.getParameter("sort");
//		String order = request.getParameter("order");
//		query.setOrder(order);
//		query.setSort(sort);
		PageView<${className}DO> page = ${lowerName}Service.selectPageList(query);
		return super.buildDataGrid(page);
	}

	/**
	 * 加载添加页面.
	 * 
	 * @param model
	 */
	@RequiresPermissions("${lowerName}:${lowerName}:save")
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String toAdd(Model model) {
		model.addAttribute("entity", new ${className}DO());
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_ADD);
		return FORM_PAGE;
	}

	/**
	 * 保存数据.
	 * 
	 * @param entity
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult add(@Valid ${className}DO entity, Model model, HttpServletRequest request) {
        try {
            ${lowerName}Service.save(entity);
            
            //附件管理 star
            String uploadfiledIds = request.getParameter("uploadfileIds");
            String typefiled = request.getParameter("typefiled");
            if(!"".equals(uploadfiledIds)&&uploadfiledIds.split(",").length>0)
            {
            	coFileService.saveBatch(uploadfiledIds.split(","), new Long((long)entity.getId()), typefiled);
            }
           //附件管理 end
            
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 加载修改页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("${lowerName}:${lowerName}:change")
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {	
		updateView(id, model);
		return FORM_PAGE;
	}

	/**
	 * 修改数据.
	 * 
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult update(@Valid @ModelAttribute @RequestBody ${className}DO entity, Model model, HttpServletRequest request) {
        try {
            ${lowerName}Service.save(entity);           
            //附件管理 star
            String uploadfiledIds = request.getParameter("uploadfileIds");
            //附件的类型，需要在页面确定属性<input type="hidden" name="typefiled" id="typefiled" value="community"/>  
            String typefiled = request.getParameter("typefiled");
            if(!"".equals(uploadfiledIds)&&uploadfiledIds.split(",").length>0)
            {
            	coFileService.updateBatch(uploadfiledIds.split(","), new Long((long)entity.getId()), typefiled);
            }
           //附件管理 end       
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}

	/**
	 * 删除数据.
	 * 
	 * @param id
	 * @return
	 */
	@RequiresPermissions("${lowerName}:${lowerName}:remove")
	@RequestMapping(value = "remove/{id}")
	@ResponseBody
	public JsonResponseResult remove(@PathVariable("id") Long id) {
	    try {
            if (!NumberUtils.isEmptyLong(id)) {
                ${lowerName}Service.remove(id);
            }
            return JsonResponseResult.success();

        }catch (ServiceException e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
        }
	}
	
	/**
	 * 加载查看页面.
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("${lowerName}:${lowerName}:view")
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, Model model) {
		model.addAttribute("view", "true");
		updateView(id, model);

		return FORM_PAGE;
	}
	
	/**
	 * 导出信息
	 * @param ids
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("${lowerName}:${lowerName}:export")
	@RequestMapping("export")
	@ResponseBody
	public String export(${className}DO query,HttpServletRequest request,HttpServletResponse response){
		TableCustomTempletDO entity = new TableCustomTempletDO();
		entity.setJavaBean("${className}DO");
		List<TableCustomTempletDO> tableCustomTempletlist = tableCustomTempletService.queryList(entity);
		TableCustomTempletDO tableCustomTempletDO = new TableCustomTempletDO();
		if(tableCustomTempletlist.size()>0)
		{
			tableCustomTempletDO = tableCustomTempletlist.get(0);
		}
		else
		{
			return "未设置信息";
		}
		return ExcelUtils.export(${lowerName}Service.queryList(query), tableCustomService.findTableCustomExport(tableCustomTempletDO.getType()), tableCustomTempletDO.getName()+DateUtils.getDateRandom()+".xls", request, response);
	
	}
	
	private void updateView(Long id, Model model) {
		model.addAttribute("entity", ${lowerName}Service.getById(id));
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_UPDATE);
		//获取附件列表   start
		//提醒需要在com.kind.common.contants中加入对应的实体的常量
		// public final static String COFILE_${upperName} = "${className}DO";
		String objectB =Constants.COFILE_${upperName};
		String objectA =Constants.COFILE_FILE;
		List<FileDO> filelist =  coFileService.getBatch(objectA, objectB, id);
		StringBuffer fileIds = new StringBuffer();
		for (FileDO fileDO : filelist) {
			fileIds.append(fileDO.getId()+",");
		}
		model.addAttribute("filelist", filelist);
		model.addAttribute("fileIds", fileIds);
		//获取附件列表   end
	}

}
