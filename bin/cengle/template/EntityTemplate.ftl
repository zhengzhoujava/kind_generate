package ${bussPackage}.${lowerName}.domain#if($!entityPackage).${entityPackage}#end;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kind.common.persistence.PageQuery;
import java.util.Date;
/**
 * ${codeName}<br/>
 *
 * @Date: ${dateTime}
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public class ${classNameDO} extends PageQuery {
	
	${feilds}
}

