/*
 *
 * sql 只是创建菜单，需要进行界面化的用户的权限分配
 *
 *
 */
/*********************************开始菜单创建*************************************************/

 INSERT INTO `sys_permission` (`parent_id`, `name`, `type`, `sort_no`, `url`, `perm_code`, `icon`, `status`, `description`, `create_user`, `modify_user`, `modify_time`, `create_time`) VALUES ('71', '${codeName}', 'F', '1', '${lowerName}/${lowerName}', NULL, 'icon-hamburg-product', '1', '${codeName}', NULL, NULL, NULL, '${dateTime}');

/*********************************结束菜单创建*************************************************/

/*********************************开始按钮权限*************************************************/
INSERT INTO `sys_permission` (`parent_id`, `name`, `type`, `sort_no`, `url`, `perm_code`, `icon`, `status`, `description`, `create_user`, `modify_user`, `modify_time`, `create_time`) VALUES ('填写主菜单的id', '添加', 'O', '1', '', '${lowerName}:${lowerName}:save', NULL, '1', '添加', NULL, NULL, NULL, '${dateTime}');
INSERT INTO `sys_permission` (`parent_id`, `name`, `type`, `sort_no`, `url`, `perm_code`, `icon`, `status`, `description`, `create_user`, `modify_user`, `modify_time`, `create_time`) VALUES ('填写主菜单的id', '修改', 'O', '2', '', '${lowerName}:${lowerName}:change', NULL, '1', '修改', NULL, NULL, NULL, '${dateTime}');
INSERT INTO `sys_permission` (`parent_id`, `name`, `type`, `sort_no`, `url`, `perm_code`, `icon`, `status`, `description`, `create_user`, `modify_user`, `modify_time`, `create_time`) VALUES ('填写主菜单的id', '查看', 'O', '3', '', '${lowerName}:${lowerName}:view', NULL, '1', '查看', NULL, NULL, NULL, '${dateTime}');
INSERT INTO `sys_permission` (`parent_id`, `name`, `type`, `sort_no`, `url`, `perm_code`, `icon`, `status`, `description`, `create_user`, `modify_user`, `modify_time`, `create_time`) VALUES ('填写主菜单的id', '删除', 'O', '4', '', '${lowerName}:${lowerName}:remove', NULL, '1', '删除', NULL, NULL, NULL, '${dateTime}');
INSERT INTO `sys_permission` (`parent_id`, `name`, `type`, `sort_no`, `url`, `perm_code`, `icon`, `status`, `description`, `create_user`, `modify_user`, `modify_time`, `create_time`) VALUES ('填写主菜单的id', '导出', 'O', '5', '', '${lowerName}:${lowerName}:export', NULL, '1', '导出', NULL, NULL, NULL, '${dateTime}');
/*********************************结束按钮权限*************************************************/

/*********************************开始导出表设置*************************************************/
INSERT INTO `sys_table_custom_templet` (`type`, `name`, `javaBean`) VALUES ( '${SQL.sqltableCustomTempletCount}', '${codeName}', '${className}DO');
${SQL.tableCustomSQL}
/*********************************结束导出表设置*************************************************/