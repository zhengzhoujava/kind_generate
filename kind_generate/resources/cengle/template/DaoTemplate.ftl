package ${bussPackage}.${lowerName}.dao#if($!entityPackage).${entityPackage}#end;


import java.util.List;

import com.kind.common.persistence.PageQuery;
import ${bussPackage}.${lowerName}.domain.${classNameDO};

/**
 * Function:${codeName}数据访问接口. <br/>
 * @date:${dateTime} <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public interface ${className}Dao {

    final String NAMESPACE = "${bussPackage}.mapper.${lowerName}.${classNameDO}Mapper.";

	/**
	 * 分页查询的数据<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	List<${classNameDO}> page(PageQuery pageQuery);

	/**
	 * 分页查询的数据记录数<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	int count(PageQuery pageQuery);

	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(${classNameDO} entity);

	/**
	 * 根据id获取数据对象<br/>
	 *
	 * @param id
	 * @return
	 */
	${classNameDO} getById(Long id);

    /**
     * 删除数据 <br/>
     *
     * @param id
     */
    void remove(Long id);
    
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<${classNameDO}> queryList(${classNameDO} entity);
}
