package ${bussPackage}.${lowerName}.dao.impl#if($!entityPackage).${entityPackage}#end;


import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.kind.common.datasource.KindDataSourceHolder;
import com.kind.common.datasource.DataSourceKey;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import ${bussPackage}.${lowerName}.dao.${className}Dao;
import ${bussPackage}.${lowerName}.domain.${classNameDO};

/**
 * 
 * ${codeName}数据访问实现类. <br/>
 * 
 * @date:${dateTime} <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class ${className}DaoImpl extends BaseDaoMyBatisImpl<${classNameDO}, Serializable> implements ${className}Dao {

	@Override
	public List<${classNameDO}> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}

	@Override
	public int saveOrUpdate(${classNameDO} entity) {
		//KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
	}

	@Override
	public ${classNameDO} getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}

	@Override
	public List<${classNameDO}> queryList(${classNameDO} entity) {
		return super.query(NAMESPACE + "queryList", entity);
	}

}
