package ${bussPackage}.${lowerName}.service.impl#if($!entityPackage).${entityPackage}#end;

import java.util.List;

import com.kind.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import ${bussPackage}.${lowerName}.dao.${className}Dao;
import ${bussPackage}.${lowerName}.domain.${className}DO;
import ${bussPackage}.${lowerName}.service.${className}Service;

/**
 * 
 * ${codeName}业务处理实现类. <br/>
 *
 * @date:${dateTime} <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ${className}ServiceImpl implements ${className}Service {

	@Autowired
	private ${className}Dao ${lowerName}Dao;

	@Override
	public PageView<${classNameDO}> selectPageList(PageQuery pageQuery) {
        pageQuery.setPageSize(pageQuery.getPageSize());
		List<${classNameDO}> list = ${lowerName}Dao.page(pageQuery);
		int count = ${lowerName}Dao.count(pageQuery);
        pageQuery.setItems(count);
		return new PageView<>(pageQuery, list);
	}

	@Override
	public int save(${classNameDO} entity) throws ServiceException {
		try {
           return ${lowerName}Dao.saveOrUpdate(entity);
        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }

	}

	@Override
	public ${classNameDO} getById(Long id) {
		return ${lowerName}Dao.getById(id);
	}

    @Override
    public void remove(Long id) throws ServiceException{
        try {
            ${lowerName}Dao.remove(id);

        }catch (Exception e){
            throw new ServiceException(e.getMessage());
        }
    }

	@Override
	public List<${classNameDO}> queryList(${classNameDO} entity) {
		return ${lowerName}Dao.queryList(entity);
	}
}