package codeGenerate.def;

import java.util.ResourceBundle;

public class CodeResourceUtil
{

  //数据库配置文件
  private static final ResourceBundle bundle = ResourceBundle.getBundle("cengle/cengle_database");
  //相关包和模板的配置
  private static final ResourceBundle bundlePath = ResourceBundle.getBundle("cengle/cengle_config");
  //数据库驱动
  public static String DIVER_NAME = "";
  //数据库连接地址
  public static String URL = "";
  //用户名
  public static String USERNAME = "";
  //密码
  public static String PASSWORD = "";
  //数据库名称
  public static String DATABASE_NAME = "";
  //数据库类型
  public static String DATABASE_TYPE = "mysql";
  //mysql数据库类名名称
  public static String DATABASE_TYPE_MYSQL = "mysql";
  //oracle数据库类名名称
  public static String DATABASE_TYPE_ORACLE = "oracle";
  //jsp页面的校验数量
  public static String CENGLE_UI_FIELD_REQUIRED_NUM = "4";
  //jsp页面的查找数量
  public static String CENGLE_UI_FIELD_SEARCH_NUM = "3";
  //web服务的路径
  public static String web_root_package = "";
  //源码的路径
  public static String source_root_package = "src";
  //业务包的名称
  public static String bussiPackage = "";
  //业务包的路径
  public static String bussiPackageUrl = "sun";
  //实体包的名称
  public static String entity_package = "entity";
  //page的路径
  public static String page_package = "page";
  //实体包的完整路径
  public static String ENTITY_URL = source_root_package + "/" + bussiPackageUrl + "/" + entity_package + "/";
  //页面的完整路径
  public static String PAGE_URL = source_root_package + "/" + bussiPackageUrl + "/" + page_package + "/";
  //开发的路径
  public static String ENTITY_URL_INX = bussiPackage + "." + entity_package + ".";
  //包的路径
  public static String PAGE_URL_INX = bussiPackage + "." + page_package + ".";
  //临时的目录
  public static String TEMPLATEPATH;
  //源码的目录
  public static String CODEPATH = source_root_package + "/" + bussiPackageUrl + "/";
  //jsp的路径
  public static String JSPPATH = web_root_package + "/" + bussiPackageUrl + "/";
  
  public static String BUSSIPACKAGE = "";
  
  public static String CENGLE_GENERATE_TABLE_ID;
  public static String CENGLE_GENERATE_UI_FILTER_FIELDS;
  public static String SYSTEM_ENCODING;
  
  public static String BUILD_PATH;

  static
  {
    DIVER_NAME = getDIVER_NAME();
    URL = getURL();
    USERNAME = getUSERNAME();
    PASSWORD = getPASSWORD();
    DATABASE_NAME = getDATABASE_NAME();
    BUILD_PATH = getBuild_path();

    SYSTEM_ENCODING = getSYSTEM_ENCODING();
    TEMPLATEPATH = getTEMPLATEPATH();
    source_root_package = getSourceRootPackage();
    web_root_package = getWebRootPackage();
    web_root_package.replace(".", "/");
    bussiPackage = getBussiPackage();
    BUSSIPACKAGE = getWebPackage();
    bussiPackageUrl = bussiPackage.replace(".", "/");

    CENGLE_GENERATE_TABLE_ID = getCengle_generate_table_id();

    CENGLE_UI_FIELD_SEARCH_NUM = getCengle_ui_search_filed_num();

    if ((URL.indexOf("mysql") >= 0) || (URL.indexOf("MYSQL") >= 0))
      DATABASE_TYPE = DATABASE_TYPE_MYSQL;
    else if ((URL.indexOf("oracle") >= 0) || (URL.indexOf("ORACLE") >= 0)) {
      DATABASE_TYPE = DATABASE_TYPE_ORACLE;
    }

    source_root_package = source_root_package.replace(".", "/");
  }

  private void ResourceUtil()
  {
  }

  public static final String getDIVER_NAME()
  {
    return bundle.getString("diver_name");
  }

  public static final String getURL()
  {
    return bundle.getString("url");
  }

  public static final String getUSERNAME()
  {
    return bundle.getString("username");
  }

  public static final String getPASSWORD()
  {
    return bundle.getString("password");
  }

  public static final String getDATABASE_NAME()
  {
    return bundle.getString("database_name");
  }

  private static String getBussiPackage() {
    return bundlePath.getString("bussi_package");
  }

  public static final String getEntityPackage()
  {
    return bundlePath.getString("entity_package");
  }

  public static final String getPagePackage()
  {
    return bundlePath.getString("page_package");
  }

  public static final String getTEMPLATEPATH()
  {
    return bundlePath.getString("templatepath");
  }

  public static final String getSourceRootPackage()
  {
    return bundlePath.getString("source_root_package");
  }

  public static final String getWebRootPackage()
  {
    return bundlePath.getString("webroot_package");
  }

  public static final String getSYSTEM_ENCODING()
  {
    return bundlePath.getString("system_encoding");
  }

  public static final String getCengle_generate_table_id()
  {
    return bundlePath.getString("cengle_generate_table_id");
  }

  public static final String getCengle_generate_ui_filter_fields()
  {
    return bundlePath.getString("cengle_generate_ui_filter_fields");
  }

  public static final String getCengle_ui_search_filed_num()
  {
    return bundlePath.getString("cengle_ui_search_filed_num");
  }

  public static final String getCengle_ui_field_required_num()
  {
    return bundlePath.getString("cengle_ui_field_required_num");
  }
  
  public static final String getBuild_path()
  {
    return bundlePath.getString("build_path");
  }
  
  private static String getWebPackage() {
	    return bundlePath.getString("web_package");
	  }
}