package com.cg.entity;

import com.base.entity.BaseEntity;
/**
 * 
 * <br>
 * <b>功能：</b>TCommunityEntity<br>
 */
public class TCommunity extends BaseEntity {
	
		private java.lang.Long id;//   主键	private java.lang.String cityName;//   城市名称	private java.lang.String cityCode;//   城市编码	private java.lang.String areaName;//   区域名称	private java.lang.String areaCode;//   区域编码	private java.lang.String community;//   小区名称	private java.lang.String company;//   单位名称	private java.util.Date createDate;//   	public java.lang.Long getId() {	    return this.id;	}	public void setId(java.lang.Long id) {	    this.id=id;	}	public java.lang.String getCityName() {	    return this.cityName;	}	public void setCityName(java.lang.String cityName) {	    this.cityName=cityName;	}	public java.lang.String getCityCode() {	    return this.cityCode;	}	public void setCityCode(java.lang.String cityCode) {	    this.cityCode=cityCode;	}	public java.lang.String getAreaName() {	    return this.areaName;	}	public void setAreaName(java.lang.String areaName) {	    this.areaName=areaName;	}	public java.lang.String getAreaCode() {	    return this.areaCode;	}	public void setAreaCode(java.lang.String areaCode) {	    this.areaCode=areaCode;	}	public java.lang.String getCommunity() {	    return this.community;	}	public void setCommunity(java.lang.String community) {	    this.community=community;	}	public java.lang.String getCompany() {	    return this.company;	}	public void setCompany(java.lang.String company) {	    this.company=company;	}	public java.util.Date getCreateDate() {	    return this.createDate;	}	public void setCreateDate(java.util.Date createDate) {	    this.createDate=createDate;	}
}

