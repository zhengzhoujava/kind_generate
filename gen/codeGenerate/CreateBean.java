package codeGenerate;

import codeGenerate.def.CodeResourceUtil;
import codeGenerate.def.CommUtil;
import codeGenerate.def.TableConvert;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

import com.base.util.DateUtils;


public class CreateBean {
	private Connection connection = null;
	static String url;
	static String username;
	static String password;
	static String rt = "\r\t";
	String SQLTables = "show tables";
	private String method;
	private String argv;
	static String selectStr = "select ";
	static String from = " from ";

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setMysqlInfo(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}

	public List<String> getTables() throws SQLException {
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement(SQLTables);
		ResultSet rs = ps.executeQuery();
		List list = new ArrayList();
		while (rs.next()) {
			String tableName = rs.getString(1);
			list.add(tableName);
		}
		rs.close();
		ps.close();
		con.close();
		return list;
	}

	public List<ColumnData> getColumnDatas(String tableName) throws SQLException {
		String SQLColumns = "select column_name ,data_type,column_comment,0,0,character_maximum_length,is_nullable nullable from information_schema.columns where table_name =  '"
				+ tableName + "' " + "and table_schema =  '" + CodeResourceUtil.DATABASE_NAME + "'";

		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement(SQLColumns);
		List columnList = new ArrayList();
		ResultSet rs = ps.executeQuery();
		StringBuffer str = new StringBuffer();
		StringBuffer getset = new StringBuffer();
		while (rs.next()) {
			String name = rs.getString(1);
			String type = rs.getString(2);
			String comment = rs.getString(3);
			String precision = rs.getString(4);
			String scale = rs.getString(5);
			String charmaxLength = rs.getString(6) == null ? "" : rs.getString(6);
			String nullable = TableConvert.getNullAble(rs.getString(7));
			type = getType(type, precision, scale);

			ColumnData cd = new ColumnData();
			cd.setColumnName(name);
			cd.setDataType(type);
			cd.setColumnType(rs.getString(2));
			cd.setColumnComment(comment);
			cd.setPrecision(precision);
			cd.setScale(scale);
			cd.setCharmaxLength(charmaxLength);
			cd.setNullable(nullable);
			formatFieldClassType(cd);
			columnList.add(cd);
		}
		argv = str.toString();
		method = getset.toString();
		rs.close();
		ps.close();
		con.close();
		return columnList;
	}

	public String getBeanFeilds(String tableName) throws SQLException {
		List<ColumnData> dataList = getColumnDatas(tableName);
		StringBuffer str = new StringBuffer();
		StringBuffer getset = new StringBuffer();
		for (ColumnData d : dataList) {
			String name = codeGenerate.def.CommUtil.formatName(d.getColumnName());
			String type = d.getDataType();
			String comment = d.getColumnComment();

			String maxChar = name.substring(0, 1).toUpperCase();
			str.append("\r\t").append("/** ").append(comment).append("*/");
			str.append("\r\t").append("private ").append(type + " ").append(name).append(";");
			String method = maxChar + name.substring(1, name.length());
			if("java.util.Date".equals(type))
			{
				getset.append("\r\t").append("@JsonFormat(pattern = \"yyyy-MM-dd HH:mm:ss\")");
			}
			getset.append("\r\t").append("public ").append(type + " ").append("get" + method + "() {\r\t");
			getset.append("    return this.").append(name).append(";\r\t}");
			getset.append("\r\t").append("public void ").append("set" + method + "(" + type + " " + name + ") {\r\t");
			getset.append("    this." + name + "=").append(name).append(";\r\t}");
		}
		argv = str.toString();
		this.method = getset.toString();
		return argv + this.method;
	}

	private String formatTableName(String name) {
		String[] split = name.split("_");
		if (split.length > 1) {
			StringBuffer sb = new StringBuffer();
			for (int i = 1; i < split.length; i++) {
				String tempName = split[i].substring(0, 1).toUpperCase() + split[i].substring(1, split[i].length());
				sb.append(tempName);
			}

			return sb.toString();
		}
		String tempName = split[0].substring(0, 1).toUpperCase() + split[0].substring(1, split[0].length());
		return tempName;
	}

	private void formatFieldClassType(ColumnData columnt) {
		String fieldType = columnt.getColumnType();
		String scale = columnt.getScale();

		if ("N".equals(columnt.getNullable())) {
			columnt.setOptionType("required:true");
		}
		if (("datetime".equals(fieldType)) || ("time".equals(fieldType))) {
			columnt.setClassType("easyui-datetimebox");
		} else if ("date".equals(fieldType)) {
			columnt.setClassType("easyui-datebox");
		} else if ("int".equals(fieldType)) {
			columnt.setClassType("easyui-numberbox");
		} else if ("number".equals(fieldType)) {
			if ((StringUtils.isNotBlank(scale)) && (Integer.parseInt(scale) > 0)) {
				columnt.setClassType("easyui-numberbox");
				if (StringUtils.isNotBlank(columnt.getOptionType()))
					columnt.setOptionType(columnt.getOptionType() + "," + "precision:2,groupSeparator:','");
				else
					columnt.setOptionType("precision:2,groupSeparator:','");
			} else {
				columnt.setClassType("easyui-numberbox");
			}
		} else if (("float".equals(fieldType)) || ("double".equals(fieldType)) || ("decimal".equals(fieldType))) {
			columnt.setClassType("easyui-numberbox");
			if (StringUtils.isNotBlank(columnt.getOptionType()))
				columnt.setOptionType(columnt.getOptionType() + "," + "precision:2,groupSeparator:','");
			else
				columnt.setOptionType("precision:2,groupSeparator:','");
		} else {
			columnt.setClassType("easyui-validatebox");
		}
	}

	public String getType(String dataType, String precision, String scale) {
		dataType = dataType.toLowerCase();
		if (dataType.contains("char")||dataType.contains("text"))
			dataType = "java.lang.String";
		else if (dataType.contains("bit"))
			dataType = "java.lang.Boolean";
		else if (dataType.contains("bigint"))
			dataType = "java.lang.Long";
		else if (dataType.contains("int"))
			dataType = "java.lang.Integer";
		else if (dataType.contains("float"))
			dataType = "java.lang.Float";
		else if (dataType.contains("double"))
			dataType = "java.lang.Double";
		else if (dataType.contains("number")) {
			if ((StringUtils.isNotBlank(scale)) && (Integer.parseInt(scale) > 0))
				dataType = "java.math.BigDecimal";
			else if ((StringUtils.isNotBlank(precision)) && (Integer.parseInt(precision) > 6))
				dataType = "java.lang.Long";
			else
				dataType = "java.lang.Integer";
		} else if (dataType.contains("decimal"))
			dataType = "BigDecimal";
		else if (dataType.contains("date"))
			dataType = "java.util.Date";
		else if (dataType.contains("time"))
			dataType = "java.sql.Timestamp";
		else if (dataType.contains("clob"))
			dataType = "java.sql.Clob";
		else {
			dataType = "java.lang.Object";
		}
		return dataType;
	}

	public void getPackage(int type, String createPath, String content, String packageName, String className, String extendsClassName, String[] importName) throws Exception {
		if (packageName == null) {
			packageName = "";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("package ").append(packageName).append(";\r");
		sb.append("\r");
		for (int i = 0; i < importName.length; i++) {
			sb.append("import ").append(importName[i]).append(";\r");
		}
		sb.append("\r");
		sb.append("/**\r *  entity. @author wolf Date:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\r */");
		sb.append("\r");
		sb.append("\rpublic class ").append(className);
		if (extendsClassName != null) {
			sb.append(" extends ").append(extendsClassName);
		}
		if (type == 1)
			sb.append(" ").append("implements java.io.Serializable {\r");
		else {
			sb.append(" {\r");
		}
		sb.append("\r\t");
		sb.append("private static final long serialVersionUID = 1L;\r\t");
		String temp = className.substring(0, 1).toLowerCase();
		temp = temp + className.substring(1, className.length());
		if (type == 1) {
			sb.append("private " + className + " " + temp + "; // entity ");
		}
		sb.append(content);
		sb.append("\r}");
		System.out.println(sb.toString());
		createFile(createPath, "", sb.toString());
	}

	public String getTablesNameToClassName(String tableName) {
		String tempTables = formatTableName(tableName);
		return tempTables;
	}

	public void createFile(String path, String fileName, String str) throws IOException {
		FileWriter writer = new FileWriter(new File(path + fileName));
		writer.write(new String(str.getBytes("utf-8")));
		writer.flush();
		writer.close();
	}

	public Map<String, Object> getAutoCreateSql(String tableName) throws Exception {
		Map sqlMap = new HashMap();
		List columnDatas = getColumnDatas(tableName);
		String columns = getColumnSplit(columnDatas);
		String formatColumns = getFormatColumnSplit(columnDatas);
		String[] columnList = getColumnList(columns);
		String columnFields = getColumnFields(columns);
		String insert = "insert into " + tableName + "(" + columns.replaceAll("\\|", ",") + ")\n values(#{" + formatColumns.replaceAll("\\|", "},#{") + "})";
		String update = getUpdateSql(tableName, columnList);
		String updateSelective = getUpdateSelectiveSql(tableName, columnDatas);
		String selectById = getSelectByIdSql(tableName, columnList);
		String delete = getDeleteSql(tableName, columnList);
		String insertParamer = getInsertParamerSql(tableName, columnList);
		String insertValues = getInsertValuesSql(tableName, columnList);
		
		String jspListQuery = getJspQuery(tableName, columnDatas);
		String jspListData = getJspData(tableName, columnDatas);
		
		String jspFromContent = getJspFromContent(tableName, columnDatas);
		
		String jspFromRichContent = getJspFromRichContent(tableName, columnDatas);
		
		sqlMap.put("columnList", columnList);
		sqlMap.put("columnFields", columnFields);
		sqlMap.put("insert", insert.replace("#{createTime}", "now()").replace("#{updateTime}", "now()"));
		sqlMap.put("update", update.replace("#{createTime}", "now()").replace("#{updateTime}", "now()"));
		sqlMap.put("delete", delete);
		sqlMap.put("updateSelective", updateSelective);
		sqlMap.put("selectById", selectById);
		sqlMap.put("insertParamer", insertParamer);
		sqlMap.put("insertValues", insertValues);
		
		sqlMap.put("jspListQuery", jspListQuery);
		sqlMap.put("jspListData", jspListData);
		sqlMap.put("jspFromContent", jspFromContent);
		sqlMap.put("jspFromRichContent", jspFromRichContent);
		
		String tableCustomTempletCount = getTableCustomTempletCount();
		sqlMap.put("sqltableCustomTempletCount", tableCustomTempletCount);
		
		String tableCustomSQL = getTableCustomSQL(tableCustomTempletCount,columnDatas);
		sqlMap.put("tableCustomSQL", tableCustomSQL);
		return sqlMap;
	}

	public String getDeleteSql(String tableName, String[] columnsList) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("delete ");
		sb.append("\t from ").append(tableName).append(" where ");
		sb.append(columnsList[0]).append(" = #{").append(CommUtil.formatName(columnsList[0])).append("}");
		return sb.toString();
	}

	public String getSelectByIdSql(String tableName, String[] columnsList) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("select <include refid=\"Base_Column_List\" /> \n");
		sb.append("\t from ").append(tableName).append(" where ");
		sb.append(columnsList[0]).append(" = #{").append(CommUtil.formatName(columnsList[0])).append("}");
		return sb.toString();
	}

	public String getColumnFields(String columns) throws SQLException {
		String fields = columns;
		if ((fields != null) && (!"".equals(fields))) {
			fields = fields.replaceAll("[|]", ",");
		}
		return fields;
	}

	public String[] getColumnList(String columns) throws SQLException {
		String[] columnList = columns.split("[|]");
		return columnList;
	}

	public String getUpdateSql(String tableName, String[] columnsList) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("\n\t\t\t <trim prefix=\"set\" suffix=\" where id = #{id}\" suffixOverrides=\",\" >\n");
		for (int i = 1; i < columnsList.length; i++) {
			
			String column = columnsList[i];
			if (!"CREATETIME".equals(column.toUpperCase())) {
				if ("UPDATETIME".equals(column.toUpperCase()))
					sb.append(column + "=now(),");
				else {
					sb.append("\t\t\t\t<if test=\""+CommUtil.formatName(column)+" != null\">\n");
					sb.append("\t\t\t\t\t"+column + "=#{" + CommUtil.formatName(column) + "},\n");
					sb.append("\t\t\t\t</if>");
				}
				
			}
			sb.append("\n");
		}
		sb.append("\t\t\t</trim>\n");
		String update = "update " + tableName + " \t" + sb.toString() ;
		return update;
	}

	public String getUpdateSelectiveSql(String tableName, List<ColumnData> columnList) throws SQLException {
		StringBuffer sb = new StringBuffer();
		ColumnData cd = (ColumnData) columnList.get(0);
		sb.append("\t<trim  suffixOverrides=\",\" >\n");
		for (int i = 1; i < columnList.size(); i++) {
			ColumnData data = (ColumnData) columnList.get(i);
			String columnName = data.getColumnName();
			sb.append("\t<if test=\"").append(CommUtil.formatName(columnName)).append(" != null ");

			if ("String" == data.getDataType()) {
				sb.append(" and ").append(CommUtil.formatName(columnName)).append(" != ''");
			}
			sb.append(" \">\n\t\t");
			sb.append(columnName + "=#{" + CommUtil.formatName(columnName) + "},\n");
			sb.append("\t</if>\n");
		}
		sb.append("\t</trim>");
		String update = "update " + tableName + " set \n" + sb.toString() + " where " + cd.getColumnName() + "=#{" + CommUtil.formatName(cd.getColumnName()) + "}";
		return update;
	}

	public String getColumnSplit(List<ColumnData> columnList) throws SQLException {
		StringBuffer commonColumns = new StringBuffer();
		for (ColumnData data : columnList) {
			commonColumns.append(data.getColumnName() + "|");
		}
		return commonColumns.delete(commonColumns.length() - 1, commonColumns.length()).toString();
	}

	public String getFormatColumnSplit(List<ColumnData> columnList) throws SQLException {
		StringBuffer commonColumns = new StringBuffer();
		for (ColumnData data : columnList) {
			commonColumns.append(data.getFormatColumnName() + "|");
		}
		return commonColumns.delete(commonColumns.length() - 1, commonColumns.length()).toString();
	}
	
	public String getInsertParamerSql(String tableName, String[] columnsList) throws SQLException {
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < columnsList.length; i++) {
			sb.append("\t\t\t<if test=\""+CommUtil.formatName(columnsList[i])+" != null\"> "+columnsList[i]+", </if>\n");
			
		}
		return sb.toString();
	}
	
	public String getInsertValuesSql(String tableName, String[] columnsList) throws SQLException {
		StringBuffer sb = new StringBuffer();
		
		for (int i = 1; i < columnsList.length; i++) {
			
			sb.append("\t\t\t<if test=\""+columnsList[i]+" != null \"> #{"+CommUtil.formatName(columnsList[i])+"}, </if>\n");
		}
		return sb.toString();
	}
	
	public String getJspQuery(String tableName,  List<ColumnData> columnList){
		StringBuffer sb = new StringBuffer();
		
		for (int i = 1; i < columnList.size(); i++) {
			ColumnData data = (ColumnData) columnList.get(i);
			String columnName = data.getColumnName();
			if("java.util.Date".equals(data.getDataType()))
			{
				sb.append("\t\t\t\t<input type=\"text\" name=\""+CommUtil.formatName(columnName)+"\" class=\"easyui-my97\" datefmt=\"yyyy-MM-dd\" data-options=\"width:150,prompt: '"+data.getColumnComment()+"'\"/>");
			}
			else
			{
				sb.append("\t\t\t\t<input type=\"text\" name=\""+CommUtil.formatName(columnName)+"\" class=\"easyui-validatebox\" data-options=\"width:150,prompt: '"+data.getColumnComment()+"'\"/>");
			}
			sb.append("\n");
		}
		return sb.toString();
		
	}
	
	public String getJspData(String tableName,  List<ColumnData> columnList){
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < columnList.size(); i++) {
			ColumnData data = (ColumnData) columnList.get(i);
			String columnName = data.getColumnName();
			
			if(i == 0){
				sb.append("\t\t\t\t{field:'id',title:'id',hidden:true}");
			}else{
				sb.append("\t\t\t\t{field:'"+CommUtil.formatName(columnName)+"',title:'"+data.getColumnComment()+"',width:20}");
			}
			
			if(i!=(columnList.size()-1))
			{
				sb.append(",");
			}
			
			sb.append("\n");
		}
		return sb.toString();
		
	}
	
	public String getJspFromContent(String tableName,List<ColumnData> columnList)
	{
		StringBuffer sb = new StringBuffer();
		
		for (int i = 1; i < columnList.size(); i++) {
			ColumnData data = (ColumnData) columnList.get(i);
			String columnName = data.getColumnName();
			
			sb.append("\t\t\t\t\t\t\t\t\t<tr>\n");
			sb.append("\t\t\t\t\t\t\t\t\t\t<td class=\"kv-label\">"+data.getColumnComment()+"：</td>\n");
			sb.append("\t\t\t\t\t\t\t\t\t\t<td class=\"kv-content\">\n");
			if("java.util.Date".equals(data.getDataType()))
			{
				sb.append("\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\""+CommUtil.formatName(columnName)+"\" name=\""+CommUtil.formatName(columnName)+"\"  value=\'<fmt:formatDate value=\"${entity."+CommUtil.formatName(columnName)+"}\" pattern=\"yyyy-MM-dd\" />\' class=\"easyui-my97\" datefmt=\"yyyy-MM-dd\" data-options=\"required:true, width:150,prompt: '"+data.getColumnComment()+"'\"/>\n");
			}
			else
			{
				sb.append("\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\""+CommUtil.formatName(columnName)+"\" name=\""+CommUtil.formatName(columnName)+"\" value=\"${entity."+CommUtil.formatName(columnName)+"}\" class=\"easyui-validatebox\" data-options=\"required:true,validType:['length[0,255]']\" />\n");
			}
            sb.append("\t\t\t\t\t\t\t\t\t\t</td>\n");
            sb.append("\t\t\t\t\t\t\t\t\t</tr>\n");
		}
		return sb.toString();
	}
	
	public String getJspFromRichContent(String tableName,List<ColumnData> columnList)
	{
		StringBuffer sb = new StringBuffer();
		
		for (int i = 1; i < columnList.size(); i++) {
			ColumnData data = (ColumnData) columnList.get(i);
			String columnName = data.getColumnName();
			if("text".equals(data.getColumnType()))
			{
				sb.append("var "+CommUtil.formatName(columnName)+"=KindEditor.create(\'#"+CommUtil.formatName(columnName)+"\',{width:\"500px\",height:\"180px\",items:[\'source\',\'|\',\'undo\',\'redo\',\'|\',\'preview\',\'print\',\'template\',\'code\',\'cut\',\'copy\',\'paste\',\'plainpaste\',\'wordpaste\',\'|\',\'justifyleft\',\'justifycenter\',\'justifyright\',\'justifyfull\',\'insertorderedlist\',\'insertunorderedlist\',\'indent\',\'outdent\',\'subscript\',\'superscript\',\'clearhtml\',\'quickformat\',\'selectall\',\'|\',\'fullscreen\',\'formatblock\',\'fontname\',\'fontsize\',\'|\',\'forecolor\',\'hilitecolor\',\'bold\',\'italic\',\'underline\',\'strikethrough\',\'lineheight\',\'removeformat\'],allowFileManager:true,allowImageUpload:true,afterBlur:function(){this.sync()},afterCreate:function(){},afterChange:function(){this.sync()}});");
			}
			sb.append("\n");
			
		}
		return sb.toString();
	}
	
	
	public String getTableCustomSQL(String tableCustemTempletCount,List<ColumnData> columnList)
	{
			StringBuffer sb = new StringBuffer();
		for (int i = 1; i < columnList.size(); i++) {
			ColumnData data = (ColumnData) columnList.get(i);
			String columnName = data.getColumnName();
			if("java.util.Date".equals(data.getDataType()))
			{
				sb.append("INSERT INTO `sys_table_custom` (`tb_type`, `field_sort`, `field_name`, `field_type`, `field_title`, `field_another_title`, `is_export`, `is_print`, `is_show`) VALUES ('"+tableCustemTempletCount+"', '"+i+"', '"+CommUtil.formatName(columnName)+"', '4', '"+data.getColumnComment()+"', '"+data.getColumnComment()+"', '1', '1', '1');");
			}
			else
			{
				sb.append("INSERT INTO `sys_table_custom` (`tb_type`, `field_sort`, `field_name`, `field_type`, `field_title`, `field_another_title`, `is_export`, `is_print`, `is_show`) VALUES ('"+tableCustemTempletCount+"', '"+i+"', '"+CommUtil.formatName(columnName)+"', '1', '"+data.getColumnComment()+"', '"+data.getColumnComment()+"', '1', '1', '1');");
			}
			sb.append("\n");	
		}
		return sb.toString();
	}
	
	public String  getTableCustomTempletCount() throws SQLException {
		String SQLColumns = "select count(*) from sys_table_custom_templet";

		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement(SQLColumns);
		ResultSet rs = ps.executeQuery();
		String count="0";
		while (rs.next()) {
			count = rs.getInt(1)+1+"";
		}
		rs.close();
		ps.close();
		con.close();
		return count;
	}
}