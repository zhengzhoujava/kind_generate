package codeGenerate.factory;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.base.util.DateUtils;

import codeGenerate.CommonPageParser;
import codeGenerate.CreateBean;
import codeGenerate.def.CodeResourceUtil;

public class CodeGenerateFactory
{
  private static final Log log = LogFactory.getLog(CodeGenerateFactory.class);
  private static String url = CodeResourceUtil.URL;
  private static String username = CodeResourceUtil.USERNAME;
  private static String passWord = CodeResourceUtil.PASSWORD;

  private static String buss_package = CodeResourceUtil.bussiPackage;
  private static String projectPath = getProjectPath();

  
  public static void codeGenerate(String tableName, String codeName, String controllerEntityPackage, String keyType){
	  codeGenerate(tableName, codeName,"", controllerEntityPackage, keyType);
  }
  
  
  public static void codeGenerate(String tableName, String codeName, String entityPackage, String controllerEntityPackage, String keyType)
  {
    CreateBean createBean = new CreateBean();
    //初始化数据库连接的信息
    createBean.setMysqlInfo(url, username, passWord);
    //通过表名找到对应的类名
    String className = createBean.getTablesNameToClassName(tableName);
    //通过表名找到对应的类名
    String classNameDO = className+"DO";
    //第一个字母小写
    String lowerName = className.substring(0, 1).toLowerCase() + className.substring(1, className.length());
    //源码的路径
    String srcPath = projectPath + CodeResourceUtil.source_root_package + "\\";
    //项目的路径
    String pckPath = srcPath + CodeResourceUtil.bussiPackageUrl + "\\";
    //jsp的路径
    String webPath = projectPath + CodeResourceUtil.web_root_package + "\\view\\" + CodeResourceUtil.bussiPackageUrl + "\\";
    //实体的路径
    String entityPath=(entityPackage==null||"".equals(entityPackage))?"": entityPackage + "\\";
    //控制的路径
    String cPath=(controllerEntityPackage==null||"".equals(controllerEntityPackage))?"": controllerEntityPackage + "\\";
    
    //实体的名称
    String beanPath = "domain\\" + entityPath + className + "DO.java";
    String mapperPath = "dao\\" + entityPath + className + "Dao.java";
    String mapperdaoImplPath = "dao\\impl\\" + entityPath + className + "DaoImpl.java";
    String servicePath = "service\\" + entityPath + className + "Service.java";
    String serviceImplPath = "service\\impl\\" + entityPath + className + "ServiceImpl.java";

    String controllerPath = "controller\\"+lowerName+"\\" +entityPath + className + "Controller.java";
    String sqlMapperPath = "mapper\\"+lowerName+"\\" + entityPath + className + "DOMapper.xml";
    String jspListPath = "jsp\\"+lowerName+"\\"+ entityPath + lowerName + "_list.jsp";
    String jspFormPath = "jsp\\"+lowerName+"\\" + entityPath + lowerName + "_form.jsp";
    String sqlPath = "sql\\" + lowerName + ".sql";

    
    webPath = webPath + entityPath;

    String jspPath = "views\\"+lowerName+"\\"+lowerName + ".jsp";

    VelocityContext context = new VelocityContext();
    context.put("className", className);
    context.put("classNameDO", classNameDO);
    context.put("lowerName", lowerName);
    context.put("upperName", lowerName.toUpperCase());
    context.put("codeName", codeName);
    context.put("dateTime", DateUtils.formatDateTime(new Date()));
    context.put("codeName", codeName);
    context.put("tableName", tableName);
    context.put("bussPackage", buss_package);
    context.put("entityPackage", entityPackage==""?null:entityPackage);
    context.put("controllerEntityPackage", controllerEntityPackage==""?null:controllerEntityPackage);
    context.put("keyType", keyType);
    context.put("fuhao", "#");
    context.put("web_package", CodeResourceUtil.BUSSIPACKAGE);
    try
    {
      context.put("feilds", createBean.getBeanFeilds(tableName));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try
    {
      Map sqlMap = createBean.getAutoCreateSql(tableName);
      context.put("columnDatas", createBean.getColumnDatas(tableName));
      context.put("SQL", sqlMap);
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
    context.put("jquery", "$");
    CommonPageParser.WriterPage(context, "EntityTemplate.ftl", pckPath+"//"+lowerName+"//", beanPath);
    CommonPageParser.WriterPage(context, "DaoTemplate.ftl", pckPath+"//"+lowerName+"//", mapperPath);
    CommonPageParser.WriterPage(context, "DaoImplTemplate.ftl", pckPath+"//"+lowerName+"//", mapperdaoImplPath);
    CommonPageParser.WriterPage(context, "ServiceTemplate.ftl", pckPath+"//"+lowerName+"//", servicePath);
    CommonPageParser.WriterPage(context, "ServiceImplTemplate.ftl", pckPath+"//"+lowerName+"//", serviceImplPath);
    CommonPageParser.WriterPage(context, "MapperTemplate.xml", pckPath, sqlMapperPath);
    CommonPageParser.WriterPage(context, "ControllerTemplate.ftl", pckPath, controllerPath);
    CommonPageParser.WriterPage(context, "JspFormTemplate.ftl", pckPath, jspFormPath);
    CommonPageParser.WriterPage(context, "JspListTemplate.ftl", pckPath, jspListPath);
    
    CommonPageParser.WriterPage(context, "SQL.ftl", pckPath, sqlPath);

    log.info("----------------------------\u4EE3\u7801\u751F\u6210\u5B8C\u6BD5---------------------------");
  }

  public static String getProjectPathS()
  {
    String path = System.getProperty("user.dir").replace("\\", "/") + "/";
//    String path ="D://";
    return path;
  }
  public static String getProjectPath()
  {
//    String path = System.getProperty("user.dir").replace("\\", "/") + "/";
	  String path =CodeResourceUtil.BUILD_PATH;
    return path;
  }
}